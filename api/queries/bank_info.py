from queries.client import MongoQueries
from bson.objectid import ObjectId
from models import BankIn
from queries.accounts import DuplicateAccountError


class BankInfoQueries(MongoQueries):
    collection_name = "bank_information"

    def create(self, bank_in: BankIn, account_id: str):
        bank_info = bank_in.dict()
        bank_info["account_id"] = account_id
        self.collection.insert_one(bank_info)
        bank_info["id"] = str(bank_info["_id"])
        return bank_info

    def get_all(self, account_id: str):
        results = []
        for bank_info in self.collection.find({"account_id": account_id}):
            bank_info["id"] = str(bank_info["_id"])
            results.append(bank_info)
        return results

    def update(
        self, bank_account_id: str, bank_in: BankIn, account_id: str
    ):
        bank_info = bank_in.dict()
        bank_info["account_id"] = account_id
        result = self.collection.update_one(
            {"_id": ObjectId(bank_account_id)}, {"$set": bank_info}
        )
        if result.matched_count == 0:
            return None
        bank_info["id"] = account_id
        return bank_info
    
    def delete(self, bank_account_id: str, account_id: str):
        result = self.collection.delete_one(
            {"_id": ObjectId(bank_account_id), "account_id": account_id}
        )
        return result.deleted_count > 0