from pydantic import BaseModel
from typing import List, Optional
from jwtdown_fastapi.authentication import Token


class AccountIn(BaseModel):
    full_name: str
    username: str
    password: str


class AccountOut(BaseModel):
    id: str
    full_name: str
    username: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


class BankIn(BaseModel):
    balance: int
    monthly_income: Optional[int]
    monthly_expenses: Optional[int]


class BankOut(BaseModel):
    id: str
    balance: int
    monthly_income: Optional[int]
    monthly_expenses: Optional[int]


class BankOutList(BaseModel):
    bank_accounts: List[BankOut]


class DeleteStatus(BaseModel):
    deleted: bool