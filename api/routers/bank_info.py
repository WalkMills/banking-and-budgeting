from fastapi import APIRouter, Depends, HTTPException
from authenticator import authenticator
from models import BankIn, BankOut, BankOutList
from typing import List
from models import DeleteStatus
from queries.bank_info import BankInfoQueries


router = APIRouter()


@router.get("/api/bank_account", response_model=BankOutList)
def list_info(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: BankInfoQueries = Depends()
):
    return {"bank_accounts": queries.get_all(account_id=account_data["id"] )}


@router.post("/api/create/bank_account", response_model=BankOut)
def add_acocunt(
    bank_in: BankIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: BankInfoQueries = Depends()
):
    return queries.create(bank_in=bank_in, account_id=account_data["id"])

@router.put("/api/update/bank_account/{bank_account_id}")
def update_account(
    bank_account_id: str,
    bank_in: BankIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: BankInfoQueries = Depends()
):
    bank_info = queries.update(
        bank_account_id=bank_account_id,
        bank_in=bank_in,
        account_id=account_data["id"]
    )
    if bank_info is None:
        raise HTTPException(status_code=404, detail="Bank Account not found")
    return bank_info

@router.delete("/api/delete/bank_account/{bank_account_id}", response_model=DeleteStatus)
def delete_bank_account(
    bank_account_id: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: BankInfoQueries = Depends(),
):
    return {
        "deleted": queries.delete(
            bank_account_id=bank_account_id, account_id=account_data["id"]
        )
    }
