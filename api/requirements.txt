fastapi[all]==0.92.0
uvicorn[standard]==0.17.6
pymongo[srv]==4.2.0
pytest
jwtdown-fastapi==0.5.0
requests==2.31.0


