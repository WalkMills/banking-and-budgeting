import { configureStore } from "@reduxjs/toolkit"
import { setupListeners } from "@reduxjs/toolkit/query"
import { bankApi } from "./apiSlice"


export const store = configureStore({
    reducer: {
      [bankApi.reducerPath]: bankApi.reducer  
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(bankApi.middleware)
}) 

setupListeners(store.dispatch)