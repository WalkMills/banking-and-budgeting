import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';


export const bankApi = createApi({
    reducerPath: "bankApi",
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8000'
    }),
    endpoints: (builder) => ({
        getBankInfo: builder.query({
            query: () => ({
                url: "/api/bank_account",
                credentials: "include"
            }),
            transformResponse: (response) => response.bank_accounts,
            providesTags: ["BankAccount"]
        }),
        CreateBankAccount: builder.mutation({
            query: (body) => ({
                url: '/api/create/bank_account/',
                method: 'post',
                body: body,
                credentials: "include"
            }),
            invalidatesTags: ["BankAccount"]
        }),
        updateBankInfo: builder.mutation({
            query: (bank_account_id) => ({
                url: `/api/update/bank_account/${bank_account_id}`,
                method: 'put',
                credentials: "include"
            }),
            invalidatesTags: ["BankAccount"]
        }),
        deleteBankAccount: builder.mutation({
            query: (bank_account_id) => ({
                url: `/api/delete/bank_account/${bank_account_id}`,
                method: 'delete',
                credentials: "include"          
            }),
            invalidatesTags: ["BankAccount"]
        }),
        signup: builder.mutation({
            query: (body) => ({
                url: '/api/accounts',
                method: 'post',
                body: body,
                credentials: 'include'
            }),
            invalidatesTags: ["Accounts"]
        }),
        login: builder.mutation({
            query: ({username, password}) => {
                const body = new FormData()
                body.append('username', username)
                body.append('password', password)
                return {
                    url: '/token',
                    method: 'POST',
                    body,
                    credentials: 'include'
                }
            },
            invalidatesTags: ["Accounts", "BankAccount"]
        }),
        logout: builder.mutation({
            query: () => ({
                url: '/token',
                method: "DELETE",
                credentials: 'include'
            }),
            invalidatesTags: ["Accounts", "BankAccount"]
        })
    })
})

export const {
    useGetBankInfoQuery,
    useCreateBankAccountMutation,
    useUpdateBankInfoMutation,
    useDeleteBankAccountMutation,
    useLoginMutation,
    useSignupMutation,
    useLogoutMutation
} = bankApi;