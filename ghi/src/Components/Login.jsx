import { useLoginMutation } from "../app/apiSlice";
import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";


export const Login = () => {
    const [login, loginResult] = useLoginMutation()
    const navigate = useNavigate()
    const [formData, setFormData] = useState({
        "username": '',
        "password": ''
    })


    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        login({username: formData.username, password: formData.password})
        document.getElementById('loginForm').reset()
    }

    useEffect(() => {
        if (loginResult.isSuccess) navigate('/dashboard')
    }, [loginResult])
    return (
        <div>
            <div>
                <h1>Login</h1>
                <form onSubmit={handleSubmit} id="loginForm">
                    <div>
                        <label htmlFor="username">Username</label>
                        <input required onChange={handleChange} name="username" id="username"/>
                    </div>
                    <div>
                        <label htmlFor="password">Password</label>
                        <input required onChange={handleChange} name="password" id="password"/>
                    </div>
                    <button type="submit">Login</button>
                </form>
            </div>
        </div>
    )
}