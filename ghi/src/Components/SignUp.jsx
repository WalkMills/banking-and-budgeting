import { useSignupMutation } from "../app/apiSlice";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";


export const SignUp = () => {
    const [signup] = useSignupMutation()
    const navigate = useNavigate()
    const [formData, setFormData] = useState({
        "username": "",
        "password": ""
    })

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        signup({username: formData.username, password: formData.password})
        document.getElementById("signupForm").reset()
        navigate('/dashboard')

    }

    return (
        <div>
            <div>
                <form onSubmit={handleSubmit} id="signupForm">
                    <div>
                        <label htmlFor="username">Username</label>
                        <input onChange={handleChange} type="text" name="username" placeholder="username" id="username" required/>
                    </div>
                    <div>
                        <label htmlFor="username">Username</label>
                        <input onChange={handleChange} type="text" name="username" placeholder="username" id="username" required/>
                    </div>
                    <div>
                        <label htmlFor="password">Password</label>
                        <input onChange={handleChange} type="text" name="password" placeholder="password" id="password" required/>
                    </div>
                    <button type="submit">Sign Up</button>
                </form>
            </div>
        </div>
    )
}