import { useGetBankInfoQuery } from "../app/apiSlice"


export const Dashboard = () => {
    const {data, isLoading} = useGetBankInfoQuery();
    console.log(data)
    if (isLoading) return <div>Loading Dashboard...</div>

    return (
        <div>
            <div>
                <div>
                    <table>
                        <thead>
                            <tr>
                                <td>Balance</td>
                                <td>Monthly Income</td>
                                <td>Monthly Expenses</td>
                            </tr>
                        </thead>
                        <tbody>
                            {data.map(bank => {
                                return (
                                    <tr key={bank.id}>
                                        <td>{bank.balance}</td>
                                        <td>{bank.monthly_income}</td>
                                        <td>{bank.monthly_expenses}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}
